var m3u8 = require('m3u8');
const fs = require("fs");

const FileHound = require("filehound");

const files = FileHound.create().paths("./files/m3u").find();

files.then(function(uuu) {
        y(uuu);
});


async function y(filez) {
  let r = await Promise.all(filez.map(item => parse(item)));
   console.log(r);
}

function parse(fpath) {
    return new Promise(function(res, rej) {
        var parser = m3u8.createStream();
        var file   = fs.createReadStream(fpath);
        file.pipe(parser);
    
        parser.on('m3u', function(m3u) {
         res(m3u);
        });
    });
    
}
