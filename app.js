const csv = require('csvtojson')
const path = require('path');

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)

// Set some defaults (required if your JSON file is empty)
db.defaults({ playlists:[] }).write()


const fs = require("fs");

const FileHound = require("filehound");

const files = FileHound.create().paths("./files/csv").find();

files.then(function(uuu) {
        y(uuu);


});


async function y(filez) {
   await Promise.all(filez.map(item => yyy(item)));
   console.log("done");
}


async function yyy(u) {
    let g = path.parse(u);
       let thisSize = fs.statSync(u).size;
      if (thisSize > 100000) {

        console.log(u + " is over threshold");
      }
      else {
        let jsonArray = await csv().fromFile(u);
        let obj = {
            name: g.name,
            path: g,
            data: jsonArray
        };
        db.get('playlists').push(obj).write(); 
    }
}
